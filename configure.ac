#
# /*
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  */
#
# Macro: AC_INIT (package, version, [bug-report], [tarname], [url])
# minor version number is increased at important feature change

AC_PREREQ([2.69])
AC_INIT([gccrtl2gml], [1.0], [mooigraph@gmail.com], [gccrtl2gml.tar.gz], [https:gitlab.com/mooigraph/gccrtl2gml])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_HEADERS([config.h])

# optional gcc option can be like this AM_INIT_AUTOMAKE([-Wall])
AM_INIT_AUTOMAKE

AC_CONFIG_MACRO_DIR([m4])

AC_USE_SYSTEM_EXTENSIONS

# Add host to config.h
AC_DEFINE_UNQUOTED(USED_HOST, ["$host"], [Host system under which the program will run.])
AC_DEFINE_UNQUOTED(USED_BUILD, ["$build"], [Build system under which the program was compiled on.])

# needed to support older automake versions
AC_SUBST(abs_top_builddir)
AC_SUBST(abs_top_srcdir)

# Checks for programs.
AC_PROG_CC
AC_PROG_MAKE_SET
AC_PROG_RANLIB
AM_PROG_CC_C_O

# Gcc compiler options
AC_DEFUN([RTLLEX_WARNING],
[AC_MSG_CHECKING(whether compiler accepts $1)
AC_SUBST(WARNING_CFLAGS)
ac_save_CFLAGS="$CFLAGS"
CFLAGS="$CFLAGS $1"
AC_TRY_COMPILE(,
[int x;],
WARNING_CFLAGS="$WARNING_CFLAGS $1"
AC_MSG_RESULT(yes),
AC_MSG_RESULT(no))
CFLAGS="$ac_save_CFLAGS"])

AC_ARG_ENABLE(gcc-warnings,
              AC_HELP_STRING([--enable-gcc-warnings],
	                     [turn on lots of GCC warnings (not recommended)]),
[case "${enableval}" in
   yes|no) ;;
   *)      AC_MSG_ERROR([bad value ${enableval} for gcc-warnings option]) ;;
 esac],
              [enableval=no])
if test "${enableval}" = yes; then
  RTLLEX_WARNING(-Werror)
  AC_SUBST([WERROR_CFLAGS], [$WARNING_CFLAGS])
  WARNING_CFLAGS=
  RTLLEX_WARNING(-Wall)
  RTLLEX_WARNING(-W)
  RTLLEX_WARNING(-Wbad-function-cast)
  RTLLEX_WARNING(-Wcast-align)
  RTLLEX_WARNING(-ftrapv)
  RTLLEX_WARNING(-Wformat)
  RTLLEX_WARNING(-Wmissing-declarations)
  RTLLEX_WARNING(-Wmissing-prototypes)
  RTLLEX_WARNING(-Wnested-externs)
  RTLLEX_WARNING(-Wshadow)
  RTLLEX_WARNING(-Wstrict-prototypes)
else
  WARNING_CFLAGS=
  RTLLEX_WARNING(-Wall)
  RTLLEX_WARNING(-pedantic)
fi

# Checks for libraries.
AC_CHECK_LIB([m], [sqrt])

# Checks for header files.
AC_CHECK_HEADERS([limits.h stdlib.h string.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_MALLOC
AC_FUNC_REALLOC

# final compiler settings
AC_SUBST(PACKAGE_CFLAGS)

AC_CONFIG_FILES([Makefile src/Makefile])
AC_OUTPUT

# ST_REPORT(NAME, VALUE)
AC_DEFUN([ST_REPORT], [  m4_format([%-30s %s], [$1], [$2])])

# ST_REPORT_ARG(NAME)
AC_DEFUN([ST_REPORT_ARG], [ST_REPORT([--[$1]], [$translit([$1], -, _)])
])

# ST_REPORT_ARGS(NAMES)
AC_DEFUN([ST_REPORT_ARGS], [m4_foreach(name, [$1], [ST_REPORT_ARG(name)])
])

# ST_REPORT_FEATURE(FEATURE)
AC_DEFUN([ST_REPORT_FEATURE],
[ST_REPORT([--enable-[$1]],
[$ST_FEATURE_VAR_NAME([$1]) $ST_FEATURE_DISABLE_REASON_VAR_NAME([$1])])])

# ST_REPORT_FEATURES(FEATURES)
AC_DEFUN([ST_REPORT_FEATURES], [m4_foreach(feature, [$1], [ST_REPORT_FEATURE(feature)])
])

echo This system is `uname -a`;

cat <<EOF

$PACKAGE_NAME $VERSION is ready to be built.

The following settings will be used:

Installation prefixes
ST_REPORT_ARGS([prefix, exec-prefix])
Installation directories
ST_REPORT_ARGS([bindir, datadir, libdir, includedir])
Use "make" to compile $PACKAGE_NAME $VERSION.

use ./configure --enable-gcc-warnings to enable gcc warnings and checks

EOF

AC_MSG_RESULT([
Building programs with prefix=$prefix
  c-compiler:          $CC
  c-flags:             $CFLAGS
]);

# end
