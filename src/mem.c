
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mem.h"

void *
mymalloc (size_t n, const char *func, int line)
{
  void *ret = NULL;
  if (func)
    {
    }
  if (line)
    {
    }
  if (n == 0)
    {
      return ((void *) 0);
    }
  ret = malloc (n);
  memset (ret, 0, n);
  return ((void *) ret);
}

void
myfree (void *ptr, const char *func, int line)
{
  if (func)
    {
    }
  if (line)
    {
    }
  if (ptr)
    {
      free (ptr);
    }
  return;
}

/* End. */
