#!/bin/sh -x

#
# /*
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or
#  *  (at your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful,
#  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  *  GNU General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  */
#

# own customized flex tool lexer generator
# MYFLEX=$(HOME)/bin/myflex
MYFLEX=flex

# own customized flex tool lexer generator commandline options
# -d   == add debug code
# -8   == generate 8 bits scanner
# -f   == fast large scanner
# -R   == reentrant lexer for threaded parsing

echo "rtl lexer rtllex.l"
rm -v -f gccrtl2gml.flex.c
$MYFLEX -d -8 -f --yylineno --outfile=gccrtl2gml.flex.c rtllex.l

