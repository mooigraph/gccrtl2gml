
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RTLLEX_H
#define RTLLEX_H 1

/* token codes */
#define FN 300
#define FN_NAME 301
#define CI 305
#define CI_NAME 306

#define FILE_EOF 350

extern char *lastfname;
extern char *lastcname;
extern int yylex (void);
extern void rtllexinit (FILE *f, int debug);
extern void rtllexreset (void);
extern int rtllex_lineno (void);

#endif

/* End. */

