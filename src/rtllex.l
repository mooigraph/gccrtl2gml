
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%{

/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "rtllex.h"
#include "uniqstring.h"
#include "main.h"

%}

 /* char classes */
blanks [[:blank:]]*
digits [[:digit:]]+

	/* c identifier can start with _ or alpha followed multiple alphanumerics and _ */
identifier [[:alpha:]_][[:alnum:]_]*

identifier2 .*"symbol_ref".*\".*\"

	/* Suppress gcc warning input() defined but not used use: %option noinput.  */
%option noinput

	/* For lint purists according to flex manual use: %option nounput.  */
%option nounput

	/* skip yywrap() function */
%option noyywrap

	/* 8-bits scanner */
%option 8bit

	/* generate line number */
%option yylineno

	/* start function name */
%s fn

	/* start call instruction */
%s ci

%%

^{blanks}";; Function" {
		BEGIN (fn);
		return (FN);
	}

<fn>{identifier} {
		char buf[1024];
		char *p = NULL;
		int i = 0;
		memset (buf,0,1024);
		p = yytext;
		while ((*p))
		{
			if ((*p)==0) {
				break;
			}
			if ((*p)==EOF) {
				break;
			}
			if ((*p)=='"') {
				break;
			}
			buf[i] = (char) ((*p));
			i++;
			if (i == (1024 - 1)) {
				break;
			}
			p++;
		}
		if (buf[0]==0) {
			p = uniqstring ("nil");
		} else {
			p = uniqstring (buf);
		}
		if (option_debug) { printf("\"%s\"\n",p); }
		lastfname = p;
		BEGIN (INITIAL);
		return (FN_NAME);
	}

^{blanks}"(call_insn " {
		BEGIN(ci);
		return (CI);
	}

<ci>{identifier2} {
		char buf[1024];
		char *p = NULL;
		int i = 0;
		memset (buf,0,1024);
		p = yytext;
		while ((*p))
		{
			if ((*p)==0) {
				break;
			}
			if ((*p)==EOF) {
				break;
			}
			/* find the first " */
			if ((*p)=='"') {
				p++;
				while ((*p))
				{
					if ((*p)==0) {
						break;
					}
					if ((*p)==EOF) {
						break;
					}
					/* find the last " */
					if ((*p)=='"') {
						break;
					}
					/* copy name in buf */
					buf[i] = (char)(*p);
					i++;
					/* check if buffer full */
					if (i == (1024 - 1)) {
						break;
					}
					p++;
				}
			}
			/* see if data in buf */
			if (buf[0]!=0) {
				break;
			}
			p++;
		}
		/* if no data in buffer */
		if (buf[0]==0) {
			p = uniqstring ("nil");
		} else {
			p = uniqstring (buf);
		}
		if (option_debug) { printf("\"%s\"\n",p); }
		lastcname = p;
		BEGIN (INITIAL);
		return (CI_NAME);
	}

\n	{ /* omit others and yylineno increased */ }

<<EOF>> { return (FILE_EOF); }

.	{ /* omit others */ }

%%

/* c source */

char *lastfname = NULL;
char *lastcname = NULL;

/* start lexer */
void rtllexinit (FILE *f, int lexerdebug)
{
 yyin = f;
 yy_flex_debug = lexerdebug;
 return;
}

/* clear lexer */
void rtllexreset (void)
{
 yylex_destroy ();
 yy_flex_debug = 0;
 return;
}

/* get current line number */
int rtllex_lineno (void)
{
 return (yylineno);
}

/* End. */
