/* A splay-tree datatype.  
   Copyright (C) 1998 Free Software Foundation, Inc.
   Contributed by Mark Mitchell (mark@markmitchell.com).

This file is part of GNU CC.
   
GNU CC is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

GNU CC is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU CC; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/* For an easily readable description of splay-trees, see:

     Lewis, Harry R. and Denenberg, Larry.  Data Structures and Their
     Algorithms.  Harper-Collins, Inc.  1991.  */

#include <stdio.h>
#include <string.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include "splay-tree.h"
#include "mem.h"

static void splay_tree_delete_helper (splay_tree, splay_tree_node);
static void splay_tree_splay (splay_tree, splay_tree_key);
static int splay_tree_foreach_helper (splay_tree,
				      splay_tree_node,
				      splay_tree_foreach_fn, void *);

/* Deallocate NODE (a member of SP), and all its sub-trees.  */
static void
splay_tree_delete_helper (splay_tree sp, splay_tree_node node)
{

  if (node == NULL)
    {
      return;
    }

  /* recurse */
  splay_tree_delete_helper (sp, node->left);
  splay_tree_delete_helper (sp, node->right);

  /* free() key if needed */
  if (sp->delete_key)
    {
      (*sp->delete_key) (node->key);
      node->key = (splay_tree_key) 0;
    }

  /* free() value if needed */
  if (sp->delete_value)
    {
      (*sp->delete_value) (node->value);
      node->value = (splay_tree_value) 0;
    }

  myfree ((void *) node, __FUNCTION__, __LINE__);

  return;
}

/* Rotate the edge joining the left child N with its parent P.  PP is the
   grandparents' pointer to P.  */

static inline void
rotate_left (splay_tree_node * pp, splay_tree_node p, splay_tree_node n)
{
  splay_tree_node tmp = (splay_tree_node) 0;
  tmp = n->right;
  n->right = p;
  p->left = tmp;
  *pp = n;
  return;
}

/* Rotate the edge joining the right child N with its parent P.  PP is the
   grandparents' pointer to P.  */

static inline void
rotate_right (splay_tree_node * pp, splay_tree_node p, splay_tree_node n)
{
  splay_tree_node tmp = (splay_tree_node) 0;
  tmp = n->left;
  n->left = p;
  p->right = tmp;
  *pp = n;
  return;
}

/* Splay SP around KEY.  */

static void
splay_tree_splay (splay_tree sp, splay_tree_key key)
{
  int cmp1 = 0;
  int cmp2 = 0;
  splay_tree_node n = (splay_tree_node) 0;
  splay_tree_node c = (splay_tree_node) 0;

  /* <nil> splaytree */
  if ((splay_tree) 0 == sp)
    {
      return;
    }

  /* no data */
  if (sp->root == (splay_tree_node) 0)
    {
      return;
    }

  do
    {
      /* init */
      cmp1 = 0;
      cmp2 = 0;
      n = (splay_tree_node) 0;
      c = (splay_tree_node) 0;

      n = sp->root;

      cmp1 = (*sp->comp) (key, n->key);

      /* Found.  */
      if (cmp1 == 0)
	{
	  return;
	}

      /* Left or right?  If no child, then we're done.  */
      if (cmp1 < 0)
	{
	  c = n->left;
	}
      else
	{
	  c = n->right;
	}

      if (c == (splay_tree_node) 0)
	{
	  return;
	}

      /* Next one left or right?  If found or no child, we're done
         after one rotation.  */
      cmp2 = (*sp->comp) (key, c->key);

      if ((cmp2 == 0)
	  || ((cmp2 < 0) && (c->left == (splay_tree_node) 0))
	  || ((cmp2 > 0) && (c->right == (splay_tree_node) 0)))
	{
	  if (cmp1 < 0)
	    {
	      rotate_left (&sp->root, n, c);
	    }
	  else
	    {
	      rotate_right (&sp->root, n, c);
	    }
	  return;
	}

      /* Now we have the four cases of double-rotation.  */
      if ((cmp1 < 0) && (cmp2 < 0))
	{
	  rotate_left (&n->left, c, c->left);
	  rotate_left (&sp->root, n, n->left);
	}
      else if ((cmp1 > 0) && (cmp2 > 0))
	{
	  rotate_right (&n->right, c, c->right);
	  rotate_right (&sp->root, n, n->right);
	}
      else if ((cmp1 < 0) && (cmp2 > 0))
	{
	  rotate_right (&n->left, c, c->right);
	  rotate_left (&sp->root, n, n->left);
	}
      else if ((cmp1 > 0) && (cmp2 < 0))
	{
	  rotate_left (&n->right, c, c->left);
	  rotate_right (&sp->root, n, n->right);
	}
      else
	{
	  /* actually generates assembly sourcecode. */
	}
    }
  while (1);

  return;
}

/* Call FN, passing it the DATA, for every node below NODE, all of
   which are from SP, following an in-order traversal.  If FN every
   returns a non-zero value, the iteration ceases immediately, and the
   value is returned.  Otherwise, this function returns 0.  */

static int
splay_tree_foreach_helper (splay_tree sp,
			   splay_tree_node node,
			   splay_tree_foreach_fn fn, void *data)
{
  int val = 0;

  if (node == (splay_tree_node) 0)
    {
      return (0);
    }

  val = splay_tree_foreach_helper (sp, node->left, fn, data);

  if (val)
    {
      return (val);
    }

  val = (*fn) (node, data);

  if (val)
    {
      return (val);
    }

  return ((int) splay_tree_foreach_helper (sp, node->right, fn, data));
}

/* Allocate a new splay tree, using COMPARE_FN to compare nodes,
   DELETE_KEY_FN to deallocate keys, and DELETE_VALUE_FN to deallocate
   values.  */

splay_tree
splay_tree_new (splay_tree_compare_fn compare_fn,
		splay_tree_delete_key_fn delete_key_fn,
		splay_tree_delete_value_fn delete_value_fn)
{
  splay_tree sp =
    (splay_tree) mymalloc (sizeof (struct splay_tree_), __FUNCTION__,
			   __LINE__);
  sp->root = NULL;
  sp->comp = compare_fn;
  sp->delete_key = delete_key_fn;
  sp->delete_value = delete_value_fn;

  return ((splay_tree) sp);
}

/* Deallocate SP.  */

void
splay_tree_delete (splay_tree sp)
{
  splay_tree_delete_helper (sp, sp->root);
  myfree ((void *) sp, __FUNCTION__, __LINE__);
  return;
}

/* Insert a new node (associating KEY with DATA) into SP.  If a
   previous node with the indicated KEY exists, its data is replaced
   with the new value.  */

void
splay_tree_insert (splay_tree sp, splay_tree_key key, splay_tree_value value)
{
  int comparison = 0;
  splay_tree_node node = NULL;

  splay_tree_splay (sp, key);

  if (sp->root)
    {
      comparison = (*sp->comp) (sp->root->key, key);
    }

  if (sp->root && (comparison == 0))
    {
      /* If the root of the tree already has the indicated KEY, just
         replace the value with VALUE.  */
      if (sp->delete_value)
	{
	  (*sp->delete_value) (sp->root->value);
	}
      sp->root->value = value;
    }
  else
    {
      /* Create a new node, and insert it at the root.  */
      node =
	(splay_tree_node) mymalloc (sizeof (struct splay_tree_node_),
				    __FUNCTION__, __LINE__);
      node->key = key;
      node->value = value;

      if (sp->root == NULL)
	{
	  node->left = NULL;
	  node->right = NULL;
	}
      else if (comparison < 0)
	{
	  node->left = sp->root;
	  node->right = node->left->right;
	  node->left->right = NULL;
	}
      else
	{
	  node->right = sp->root;
	  node->left = node->right->left;
	  node->right->left = NULL;
	}

      sp->root = node;
    }

  return;
}

/* insert and allow duplicates of key */
void
splay_tree_insert_duplicates (splay_tree sp, splay_tree_key key,
			      splay_tree_value value)
{
  int comparison = 0;
  splay_tree_node node = NULL;

  splay_tree_splay (sp, key);

  if (sp->root)
    {
      comparison = (*sp->comp) (sp->root->key, key);
    }

  /* Create a new node, and insert it at the root.  */
  node =
    (splay_tree_node) mymalloc (sizeof (struct splay_tree_node_),
				__FUNCTION__, __LINE__);
  node->key = key;
  node->value = value;

  if (sp->root == NULL)
    {
      node->left = NULL;
      node->right = NULL;
    }
  else if (comparison < 0)
    {
      node->left = sp->root;
      node->right = node->left->right;
      node->left->right = NULL;
    }
  else
    {
      /* > or == */
      node->right = sp->root;
      node->left = node->right->left;
      node->right->left = NULL;
    }

  sp->root = node;

  return;
}

/* Remove KEY from SP.  It is not an error if it did not exist.  */
void
splay_tree_remove (splay_tree sp, splay_tree_key key)
{
  splay_tree_node node = (splay_tree_node) 0;
  splay_tree_node left = (splay_tree_node) 0;
  splay_tree_node right = (splay_tree_node) 0;

  /* */
  splay_tree_splay (sp, key);

  /* */
  if (sp->root && ((*sp->comp) (sp->root->key, key) == 0))
    {
      node = sp->root;
      left = sp->root->left;
      right = sp->root->right;

      /* One of the children is now the root.  Doesn't matter much
         which, so long as we preserve the properties of the tree.  */
      if (left)
	{
	  sp->root = left;

	  /* If there was a right child as well, hang it off the 
	     right-most leaf of the left child.  */
	  if (right)
	    {
	      while (left->right)
		{
		  left = left->right;
		}
	      left->right = right;
	    }
	}
      else
	{
	  sp->root = right;
	}

      /* free() key if needed */
      if (sp->delete_key)
	{
	  (*sp->delete_key) (node->key);
	  node->key = (splay_tree_key) 0;
	}

      /* free() value if needed */
      if (sp->delete_value)
	{
	  (*sp->delete_value) (node->value);
	  node->value = (splay_tree_value) 0;
	}

      /* free() node itself */
      myfree ((void *) node, __FUNCTION__, __LINE__);
    }

  return;
}

/* Lookup KEY in SP, returning VALUE if present, and NULL
   otherwise.  */
splay_tree_node
splay_tree_lookup (splay_tree sp, splay_tree_key key)
{

  /* */
  splay_tree_splay (sp, key);

  if (sp->root && ((*sp->comp) (sp->root->key, key) == 0))
    {
      return ((splay_tree_node) sp->root);
    }
  else
    {
      return ((splay_tree_node) 0);
    }
}

/* Call FN, passing it the DATA, for every node in SP, following an
   in-order traversal.  If FN every returns a non-zero value, the
   iteration ceases immediately, and the value is returned.
   Otherwise, this function returns 0.  */

int
splay_tree_foreach (splay_tree sp, splay_tree_foreach_fn fn, void *data)
{
  return ((int) splay_tree_foreach_helper (sp, sp->root, fn, data));
}

/* Return the node in SP with the greatest key.  */
splay_tree_node
splay_tree_max (splay_tree sp)
{
  splay_tree_node n = (splay_tree_node) 0;

  /* <nil> splaytree */
  if ((splay_tree) 0 == sp)
    {
      return ((splay_tree_node) 0);
    }

  n = sp->root;

  /* no data */
  if (n == (splay_tree_node) 0)
    {
      return ((splay_tree_node) 0);
    }

  /* scan r */
  while (n->right)
    {
      n = n->right;
    }

  return ((splay_tree_node) n);
}

/* Return the node in SP with the smallest key.  */
splay_tree_node
splay_tree_min (splay_tree sp)
{
  splay_tree_node n = (splay_tree_node) 0;

  /* <nil> splaytree */
  if ((splay_tree) 0 == sp)
    {
      return ((splay_tree_node) 0);
    }

  n = sp->root;

  /* no data */
  if ((splay_tree_node) 0 == n)
    {
      return ((splay_tree_node) 0);
    }

  /* scan l */
  while (n->left)
    {
      n = n->left;
    }

  return ((splay_tree_node) n);
}

/* Splay-tree comparison function, treating the keys as ints.  */

int
splay_tree_compare_ints (splay_tree_key k1, splay_tree_key k2)
{
  if ((int) k1 < (int) k2)
    {
      return ((int) -1);
    }
  else if ((int) k1 > (int) k2)
    {
      return (1);
    }
  else
    {
      return (0);
    }
}

/* Splay-tree comparison function, treating the keys as pointers.  */

int
splay_tree_compare_pointers (splay_tree_key k1, splay_tree_key k2)
{
  if ((char *) k1 < (char *) k2)
    {
      return ((int) -1);
    }
  else if ((char *) k1 > (char *) k2)
    {
      return (1);
    }
  else
    {
      return (0);
    }
}

/* Comparison function for a splay tree in which the keys are strings.
   K1 and K2 have the dynamic type "const char *".  Returns <0, 0, or
   >0 to indicate whether K1 is less than, equal to, or greater than
   K2, respectively.  */

int
splay_tree_compare_strings (splay_tree_key k1, splay_tree_key k2)
{
  const char *s1 = (const char *) k1;
  const char *s2 = (const char *) k2;
  int ret = 0;

  if (s1 == (char *) 0)
    {
      return (0);
    }

  if (s2 == (char *) 0)
    {
      return (0);
    }

  /* check if same pointer */
  if (s1 == s2)
    {
      return (0);
    }

  ret = strcmp (s1, s2);

  return ((int) ret);
}

/* end */
