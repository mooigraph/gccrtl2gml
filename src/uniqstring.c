
/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "main.h"
#include "splay-tree.h"
#include "mem.h"
#include "uniqstring.h"

/* database with the strings */
static splay_tree gustring = NULL;

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void uniqstring_splay_tree_delete_key_fn (splay_tree_key keydata);

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void uniqstring_splay_tree_delete_value_fn (splay_tree_value
						   valuedata);

/* The type of a function used to deallocate any resources associated
   with the key.  */
static void
uniqstring_splay_tree_delete_key_fn (splay_tree_key keydata)
{
  char *k = NULL;
  size_t len = 0;
  /* key is a (char *) malloced */
  if (keydata)
    {
      k = (char *) keydata;
      len = strlen (k);
      memset (k, 0, len);
      myfree ((void *) keydata, __FUNCTION__, __LINE__);
    }
  return;
}

/* The type of a function used to deallocate any resources associated
   with the value.  */
static void
uniqstring_splay_tree_delete_value_fn (splay_tree_value valuedata)
{
  /* value is unused */
  if (valuedata)
    {
    }
  return;
}

/* clear all data */
void
uniqstring_clear (void)
{
  if (gustring)
    {
      splay_tree_delete (gustring);
    }
  gustring = NULL;
  return;
}

char *
uniqstring (char *s)
{
  char *buf = NULL;
  splay_tree_node spn = (splay_tree_node) 0;

  if (s == NULL)
    {
      return (NULL);
    }

  if (strlen (s) == 0)
    {
      return ((char *) "");
    }

  if (gustring == NULL)
    {
      /* indexed on (char *) */
      gustring = splay_tree_new (splay_tree_compare_strings,	/* splay_tree_compare_fn */
				 uniqstring_splay_tree_delete_key_fn,	/* splay_tree_delete_key_fn */
				 uniqstring_splay_tree_delete_value_fn	/* splay_tree_delete_value_fn */
	);
    }

  /* check if in database */
  spn = splay_tree_lookup ((splay_tree) gustring, (splay_tree_key) s);

  /* found */
  if (spn)
    {
      return ((char *) spn->key);
    }

  /* create fresh copy */
  buf = mymalloc (strlen (s) + 1, __FUNCTION__, __LINE__);
  memset (buf, 0, strlen (s) + 1);

  /* copy (dest, src, n); strcpy() is a safety bug. */
  (void) memmove ((void *) buf, (const void *) s, (size_t) strlen (s));

  /* add to database */
  splay_tree_insert (gustring,	/* splay_tree */
		     (splay_tree_key) buf,	/* index */
		     (splay_tree_value) 0	/* unused */
    );

  return (buf);
}

/* end */
